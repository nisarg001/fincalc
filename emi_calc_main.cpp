#include <iostream>
#include <fstream>
#include <getopt.h>
#include <stdint.h>
#include <math.h>
#include <vector>

using namespace std;

#define STR_LEN 256

void sprint_inr(double inr, char *str)
{
    double v10p2 = 100.000;
    uint64_t v10p3 = 1000;
    //uint64_t v10p4 = 10000;
    uint64_t v10p5 = 100000;
    //uint64_t v10p6 = 1000000;
    uint64_t v10p7 = 10000000;
    //uint64_t v10p9 = 1000000000;
    int neg = 0;

    if (inr > UINT64_MAX) {
        cout << " WARNING: Very wealthy" << endl;
    }
    if (inr < 0 ) {
        neg = 1;
        inr = -inr;
    }

    double fraction = inr - (uint64_t) inr;
    uint32_t  paisa2 = (uint64_t) (fraction* v10p2);
    uint32_t hundreds = inr - (((uint64_t) inr / v10p3) * v10p3);
    uint32_t thousands = (inr - (((uint64_t) inr / v10p5) * v10p5)) / v10p3;
    uint32_t lakhs = (inr - (((uint64_t) inr / v10p7) * v10p7)) / v10p5;
    uint32_t crores = ((uint64_t) (inr) / v10p7) % 100;
    uint32_t arabs = ((uint64_t) (inr) / v10p7) / v10p2;

//    cout << arabs  << endl;
//    cout << crores << endl;
//    cout << lakhs << endl;
//    cout << thousands << endl;
//    cout << hundreds << endl;
//    cout << paisa2 << endl;
    if (neg == 0) {
        snprintf(str, STR_LEN, "%2d,%02d,%02d,%02d,%03d.%02d", arabs, crores, lakhs, thousands, hundreds, paisa2);
    } else {
        snprintf(str, STR_LEN, "-%1d,%02d,%02d,%02d,%03d.%02d", arabs, crores, lakhs, thousands, hundreds, paisa2);
    }
}

typedef struct {
    int index;
    double interest;
    double principle;
    double total_interest;
    double total_principle;
    double balance_principle;
}components;

void usage_emi_calc(void)
{
    cout << "EMI vs Returns Calculator " << endl; 
    cout << "   ./emi_calc.out [<-r interest_rate>] [<-R return_Rate>] "
            "[<-t tenure_years>] [<-c capital_or_loan_amount>] [<-h/-H>]" << endl;
    exit(1);
}

class emi_calc {
    double rate_yearly;
    double tenure ;
    double total_capital;
    double returns;
    double emi;

    vector<components> monthly;
    vector<components> years;

public:

    emi_calc(double r, double t, double c, double R)
    {
        rate_yearly = r;
        tenure = t;
        total_capital = c;
        returns = R;
        emi = 0;
    }

    ~emi_calc()
    {
        return;
    }

    double compute_emi(int verbose)
    {
        double rate_monthly = rate_yearly / 12;   //monthly
        double tenure_months = tenure * 12;     //months
        char str_emi[STR_LEN] = {0};

        rate_monthly = rate_monthly / 100;
        double x = pow(1  + rate_monthly, tenure_months);
        emi = total_capital * rate_monthly * x / (x -1);

        if (verbose) {
            sprint_inr(emi, str_emi);
            cout << " a, c, l, t,  h. p" << endl;
            cout << str_emi << endl;
        }
        return emi;
    }


    void compute_monthly_components(int verbose)
    {
        double total_outflow = 0;
        double balance_principle = total_capital;
        double rate_monthly = rate_yearly / 12;   //monthly
        double tenure_months = tenure * 12;     //months
        double interest_outflow = 0;
        double principle_outflow = 0;
        char str_emi[STR_LEN] = {0};
        char str_outflow[STR_LEN] = {0};
        char str_balance[STR_LEN] = {0};
        char str_interest[STR_LEN] = {0};
        char str_principle[STR_LEN] = {0};
        //char str_total_interst[STR_LEN] = {0};
        //char str_total_principle[STR_LEN] = {0};

        //monthly EMI, interest, principle, balance etc
        for (int i = 0; i< (int) tenure_months; ++i) {
            total_outflow += emi;

    //        if (i > 60) {
    //            r = 7.4 / 12;
    //        }

            double interest = balance_principle * rate_monthly / 100;
            double deduce = emi - interest;
            interest_outflow += interest;
            balance_principle -= deduce;
            principle_outflow += deduce;
            sprint_inr(emi, str_emi);
            sprint_inr(total_outflow, str_outflow);
            sprint_inr(interest, str_interest);
            sprint_inr(deduce, str_principle);
            sprint_inr(balance_principle, str_balance);

            if(verbose) {
                printf("%-4d(%2d) %s %s %s %s %s\n",
                    i+1, (i/12) + 1,
                    str_emi, str_outflow, str_interest, str_principle, str_balance);
            }

            components temp;
            temp.index = i+1;
            temp.interest = interest;
            temp.principle = deduce;
            temp.balance_principle = balance_principle;
            temp.total_interest = interest_outflow;
            temp.total_principle = principle_outflow;
            monthly.push_back(temp);
        }
    }


    void compute_yearly_components(int verbose)
    {

        char str_yearly_return[STR_LEN] = {0};
        char str_yearly_outflow[STR_LEN] = {0};
        char str_net_capital[STR_LEN] = {0};
        char str_emi_compunding[STR_LEN] = {0};
        char str_just_compunding[STR_LEN] = {0};
        char str_emi[STR_LEN] = {0};
        //char str_outflow[STR_LEN] = {0};
        char str_balance[STR_LEN] = {0};
        char str_interest[STR_LEN] = {0};
        char str_principle[STR_LEN] = {0};
        char str_total_interst[STR_LEN] = {0};
        char str_total_principle[STR_LEN] = {0};
        sprint_inr(emi, str_emi);

        if (verbose) {
            cout << "Yearly " << endl;
        }

        if (verbose) {
            printf("Yr  Interest           "
                        "Principle          "
                        "Outflow            "
                        "Balance            "
                        "Total_Int          "
                        "Total_Prin         "
                        "Return             "
                        "Net_Cap           "
                        "Flag  "
                        "EMI_Compounding   "
                        "Just_Compunding   \n");
        }

        double net_capital = total_capital;
        double emi_compunding = 0;
        double just_compounding = total_capital;
        for (int i = 0; i < (int) tenure; ++i) {
            components temp_year;
            temp_year.index = i+1;
            temp_year.interest = 0;
            temp_year.principle = 0;
            for (int j = 0; j<12; ++j) {
                temp_year.interest += monthly[i*12+j].interest;
                temp_year.principle += monthly[i*12+j].principle;
                temp_year.balance_principle = monthly[i*12+j].balance_principle;
                temp_year.total_interest = monthly[i*12+j].total_interest;
                temp_year.total_principle = monthly[i*12+j].total_principle;
            }

            double yearly_outflow = temp_year.interest + temp_year.principle;
            double yearly_return = (net_capital - yearly_outflow) * returns / 100;
            net_capital = net_capital - yearly_outflow + yearly_return;

            //returns if loan is paid in advaced and EMI amount is invested in the funds
            emi_compunding += emi * 12;
            emi_compunding += emi_compunding * returns / 100;

            just_compounding += (just_compounding * returns / 100);

            sprint_inr(temp_year.interest, str_interest);
            sprint_inr(temp_year.principle, str_principle);
            sprint_inr(temp_year.balance_principle, str_balance);
            sprint_inr(temp_year.total_interest, str_total_interst);
            sprint_inr(temp_year.total_principle, str_total_principle);
            sprint_inr(yearly_return, str_yearly_return);
            sprint_inr(net_capital, str_net_capital);
            sprint_inr(yearly_outflow, str_yearly_outflow);
            sprint_inr(emi_compunding, str_emi_compunding);
            sprint_inr(just_compounding, str_just_compunding);

            int flag = 1;
            if (temp_year.balance_principle > net_capital) {
                flag = -1;
            }

            if (verbose) {
                printf("%-2d %s %s %s %s %s %s %s %s %03d  %s %s\n",
                        i+1, str_interest, str_principle, str_yearly_outflow, str_balance,
                        str_total_interst, str_total_principle,
                        str_yearly_return, str_net_capital, flag,
                        str_emi_compunding, str_just_compunding);
            } //if verbose
        } //for loop
    } //yearly function

}; //class ends


int main(int argc, char *argv[])
{
    //input arguments
    double rate_yearly = 8;
    double tenure = 20;
    double total_capital = 100000;
    double returns = 10;

    int opt;
    while((opt = getopt(argc, argv, "r:t:c:R:hH")) != -1) {
        switch(opt) {
            case 'r': rate_yearly          = atof(optarg);   break;
            case 't': tenure               = atof(optarg);   break;
            case 'c': total_capital        = atof(optarg);   break;
            case 'R': returns              = atof(optarg);   break;
            case 'h': usage_emi_calc();                      break;
            case 'H': usage_emi_calc();                      break;
            case '?':
                printf("unknown option: %c\n", optopt);
                usage_emi_calc();
                break;
        }
    }

    emi_calc ec(rate_yearly, tenure, total_capital, returns);
    ec.compute_emi(1);
    ec.compute_monthly_components(0);
    ec.compute_yearly_components(1);

    return 0;
}
